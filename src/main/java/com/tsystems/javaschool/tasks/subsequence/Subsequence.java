package com.tsystems.javaschool.tasks.subsequence;


import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

      /*
        *** Second possible realization (slow):
        * if (y.isEmpty() && !x.isEmpty())
            return false;
        * List result = (List) y.stream()
                              .filter(x::contains)
                              .distinct()
                              .collect(Collectors.toList());
        for (int i = 0; i < result.size(); i++)
            if (x.get(i) != result.get(i))
                return false;
         return true;

        */

      int i = 0, j = 0;
      while (i != x.size() && j != y.size()) {
          if (y.get(j) == x.get(i)) {
              i++;
              j++;
          } else {
              j++;
          }
      }
        return i == x.size();
    }
}
