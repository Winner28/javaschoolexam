package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private int[][] pyramid;
    private int height;
    private int width;


    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() > 100)
            throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);

        int current = 0;
        height = pyramidHeight(inputNumbers);
        if (height > 0) {
            width = height * 2 - 1;
            pyramid = new int[height][width];
            for (int i = 0; i < height; i++) {
                int k = height - i - 1;
                for (int l = 0; l <= i; l++) {
                    pyramid[i][k] = inputNumbers.get(current);
                    current++;
                    k+=2;
                }
            }
        }
        return pyramid;
    }

    private int pyramidHeight(final List<Integer> inputNumbers) {
        int counter = 1;
        boolean canBuild = true;
        int size = inputNumbers.size();
        for (Integer num : inputNumbers) {
            size -= counter;
            if (size < 0) {
                canBuild = false;
                break;
            } else if (size == 0) {
                height = counter;
                canBuild = true;
                break;
            }
            counter++;
        }
        return canBuild ? counter : -1;
    }
}