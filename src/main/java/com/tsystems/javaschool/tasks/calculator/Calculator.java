package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private final List<String> notation;
    private final Map<String, Integer> prior;

    public Calculator() {
        notation = new ArrayList<>();
        prior = new HashMap<>();
        prior.put("*", 2);
        prior.put("/", 2);
        prior.put("+", 1);
        prior.put("-", 1);
    }

    public String evaluate(String statement) {
        if (statement == null || statement.trim().isEmpty() || statement.contains(",") || !validStatement(statement)) {// || !statement.matches("^(\\d+[\\+\\-\\*\\/]{1})+\\d+$")) {
            return null;
        }

        return calculateNotation(getPolishNotation(statement));
    }

    private List<String> getPolishNotation(String statement) {

        String[] inputStatement = statement.trim()
                                           .split("(?<=[^0-9\\.])|(?=[^0-9\\.])");

        Stack<String> stack = new Stack<>();
        for (String current : inputStatement) {
            if (current.equals("("))
                stack.add(current);
            else if (current.equals(")")) {
                while (!stack.isEmpty()) {
                    if (!stack.lastElement()
                              .equals("(")) {
                        notation.add(stack.pop());
                    } else if (stack.lastElement()
                                    .equals("(")) {
                        stack.pop();
                        break;
                    }
                }
            } else if (prior.get(current) != null) {
                if (!stack.isEmpty()) {
                    if (prior.get(stack.lastElement()) != null
                            && prior.get(stack.lastElement()) >= prior.get(current))
                        notation.add(stack.pop());
                }
                stack.add(current);
            } else {
                notation.add(current);
            }
        }

        while (!stack.isEmpty()) {
            notation.add(stack.pop());
        }

        return notation;
    }

    private String calculateNotation(final List<String> notation) {
        Stack<Double> stack = new Stack<>();
        Pattern pattern = Pattern.compile("^[0-9]*[.]?[0-9]+$");
        for (String str : notation) {
            if (!pattern.matcher(str)
                        .find()) {
                switch (str) {
                    case "+":
                        stack.add(stack.pop() + stack.pop());
                        break;
                    case "-":
                        stack.add(-stack.pop() + stack.pop());
                        break;
                    case "*":
                        stack.add(stack.pop() * stack.pop());
                        break;
                    case "/":
                        double first;
                        if ((first = stack.pop()) == 0)
                            return null;
                        double second = stack.pop();
                        stack.add(second / first);
                        break;
                }
            } else {
                stack.add(Double.valueOf(str));
            }
        }
        double result = stack.pop();
        if (result % 1 == 0) {
            return String.valueOf((int) result);
        }

        return String.valueOf(result);
    }

    private boolean validStatement(String statement) {
        String[] inputStatement = statement.trim()
                                           .split("(?<=[^0-9\\.])|(?=[^0-9\\.])");

      /* Valid bracketChecker:
       Stack<String> bracketStack = new Stack<>();
        for (String current : inputStatement) {
            if (current.equals("(")) {
                bracketStack.add(current);

            }
            if (current.equals(")") && !bracketStack.isEmpty()) {
                String var = bracketStack.pop();
                if (!var.equals("("))
                    return false;
            }
        }

        if (!bracketStack.isEmpty()) {
            return false;
        }*/
      int bracketCounter = 0;
      for (String current : inputStatement) {
          if (bracketCounter < 0)
              return false;
          if (current.equals("("))
              bracketCounter++;
          if (current.equals(")"))
              bracketCounter--;
      }

      if (bracketCounter != 0)
          return false;

        boolean isOperator = false;
        String previous = "";

        for (String current : inputStatement) {
            if (current.matches("^[0-9]*[.]?[0-9]+$")) {
                isOperator = false;
            } else {
                if (previous.equals(")") && prior.get(current) != null
                        || prior.get(previous) != null && current.equals("(")) {
                    isOperator = true;
                    continue;
                }
                if (isOperator) {
                    return false;
                }
                isOperator = true;
            }
            previous = current;
        }

        return true;
    }

}
